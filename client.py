import serial
import sys
import time
import re
from time import sleep
from os import listdir
from os.path import isfile, join

ENABLE = True
READ_TIMEOUT = 8
USERNAME = "admin"
PASSWORD = "admin"
PROMPT_REGEX = ".*\>$"
EN_PROMPT_REGEX = ".*\#$"
RETRY_COUNT = 3
PATH_TO_PROJ_DIR = ""

def go_to_enable_mode(ser: serial.Serial, wait_time: float = 0.5):

    i = 0
    response = ""

    command_to_send = "enable"+"\r"
    ser.write(command_to_send.encode('utf-8'))
    sleep(wait_time)
    response = response + ser.read(ser.inWaiting()).decode('utf-8')
    if "Password" in response:
        ser.write((PASSWORD + "\r").encode('utf-8'))
        response = ""

    while i < RETRY_COUNT:
        sleep(wait_time)
        response = response + ser.read(ser.inWaiting()).decode('utf-8')
        x = re.search(EN_PROMPT_REGEX, response)
        if x != None:
            return
        else:   
            i=i+1

    raise Exception(f"Failed to find prompt, returned input was: {response}")

def send_to_console(ser: serial.Serial, command: str, wait_time: float = 0.5):

    print("Sending command: {}".format(command.strip()))

    i = 0
    response = ""

    command_to_send = command + "\r"
    ser.write(command_to_send.encode('utf-8'))

    while i < RETRY_COUNT:
        sleep(wait_time)
        response = response + ser.read(ser.inWaiting()).decode('utf-8')
        x = re.search(EN_PROMPT_REGEX, response)
        if x != None:
            return
        else:
            x = re.search(PROMPT_REGEX, response)     
            if x != None:
                return      
            i=i+1

    raise Exception(f"Failed to find prompt, returned input was: {response}")

def update_state(filename):
    # Create state file
    with open(PATH_TO_PROJ_DIR + '\state', 'w') as f:
        f.write(filename + '\n')

def main():

    print ("\nInitializing serial connection")

    console = serial.Serial(
        port='COM1',
        baudrate=9600,
        parity="N",
        stopbits=1,
        bytesize=8,
        timeout=READ_TIMEOUT
    )

    if not console.isOpen():
        sys.exit()

    console.write(('\r').encode('utf-8'))
    time.sleep(2)
    response = console.read(console.inWaiting()).decode('utf-8')
    if 'Username' in response:
        console.write((USERNAME + '\r').encode('utf-8'))
        time.sleep(2)
        response = console.read(console.inWaiting()).decode('utf-8')
        if 'Password' in response:
            console.write((PASSWORD + '\r').encode('utf-8'))
        time.sleep(2)

    if ENABLE:
        go_to_enable_mode(console, wait_time=2)

    # Get list of command files in the folder
    list_of_files = [f for f in listdir(PATH_TO_PROJ_DIR) if isfile(join(PATH_TO_PROJ_DIR, f))]

    for command_file in list_of_files:

        if command_file == "state":
            continue

        command_file = join(PATH_TO_PROJ_DIR, command_file)

        # Make sure switch is powered on and the user is ready to proceed.
        print("PLEASE MAKE SURE CONSOLE CABLE IS CONNECTED TO THE CORRECT SWITCH")
        print(f"Command file path: {command_file}")
        input("Press enter to proceed: ")

        # Using readlines()
        file1 = open(command_file, 'r')
        for line in file1.readlines():
            if line.startswith('#') or line == "":
                continue
            send_to_console(console, line, wait_time=2)       
        
        print(f"Connection to {console.name} closed.")
        update_state(command_file)

if __name__ == "__main__":
    main()
